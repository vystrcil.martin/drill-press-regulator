EESchema Schematic File Version 4
LIBS:DrillPressRegulatorPCB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7050 3650 0    50   ~ 0
R1 = 0.85 (Vmain - Vsupply) / (2 * Itotal)
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5CEA1AA9
P 1250 1250
F 0 "J1" H 1144 1435 50  0000 C CNN
F 1 "Conn_01x02_Female" H 1144 1344 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MC-G_02x5.08mm_Angled" H 1250 1250 50  0001 C CNN
F 3 "~" H 1250 1250 50  0001 C CNN
	1    1250 1250
	-1   0    0    -1  
$EndComp
Text GLabel 2550 1350 2    50   Input ~ 0
L
Text GLabel 1750 1350 2    50   Input ~ 0
N
Wire Wire Line
	1450 1350 1750 1350
$Comp
L Device:CP C3
U 1 1 5CEA1BB9
P 5900 3400
F 0 "C3" V 5645 3400 50  0000 C CNN
F 1 "22u/25V" V 5736 3400 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D7.5mm_P2.50mm" H 5938 3250 50  0001 C CNN
F 3 "~" H 5900 3400 50  0001 C CNN
	1    5900 3400
	0    -1   1    0   
$EndComp
Wire Wire Line
	5750 3400 5200 3400
Wire Wire Line
	5200 3400 5200 2950
Wire Wire Line
	5200 2950 5350 2950
$Comp
L Device:D D1
U 1 1 5CEA2812
P 6850 3400
F 0 "D1" H 6850 3184 50  0000 C CNN
F 1 "D" H 6850 3275 50  0000 C CNN
F 2 "Diode_THT:D_DO-15_P12.70mm_Horizontal" H 6850 3400 50  0001 C CNN
F 3 "~" H 6850 3400 50  0001 C CNN
	1    6850 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5CEA290D
P 7350 3400
F 0 "R6" V 7143 3400 50  0000 C CNN
F 1 "22k / 2W" V 7234 3400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P15.24mm_Horizontal" V 7280 3400 50  0001 C CNN
F 3 "~" H 7350 3400 50  0001 C CNN
	1    7350 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 3400 7200 3400
Text GLabel 7750 3400 2    50   Input ~ 0
L
$Comp
L Device:R_Variable R4
U 1 1 5CEA2D02
P 6650 3000
F 0 "R4" H 6542 2954 50  0000 R CNN
F 1 "1M" H 6542 3045 50  0000 R CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" V 6580 3000 50  0001 C CNN
F 3 "~" H 6650 3000 50  0001 C CNN
	1    6650 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R8
U 1 1 5CEA3298
P 7650 3000
F 0 "R8" H 7580 2954 50  0000 R CNN
F 1 "330k" H 7580 3045 50  0000 R CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7580 3000 50  0001 C CNN
F 3 "~" H 7650 3000 50  0001 C CNN
	1    7650 3000
	-1   0    0    1   
$EndComp
$Comp
L u2008:U2008B U1
U 1 1 5CEA173F
P 5900 2800
F 0 "U1" H 5900 3215 50  0000 C CNN
F 1 "U2008B" H 5900 3124 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 5900 2800 50  0001 C CNN
F 3 "" H 5900 2800 50  0001 C CNN
	1    5900 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3400 7650 3400
Wire Wire Line
	6050 3400 6450 3400
Wire Wire Line
	6450 2950 6450 3400
Connection ~ 6450 3400
Wire Wire Line
	6450 3400 6500 3400
Wire Wire Line
	6450 2850 6650 2850
Wire Wire Line
	6650 3150 6650 3400
Connection ~ 6650 3400
Wire Wire Line
	6650 3400 6700 3400
Wire Wire Line
	7650 3150 7650 3400
Connection ~ 7650 3400
Wire Wire Line
	7650 3400 7750 3400
Wire Wire Line
	6450 2750 7650 2750
Wire Wire Line
	7650 2750 7650 2850
$Comp
L Device:R R5
U 1 1 5CEACB28
P 6750 2650
F 0 "R5" V 6957 2650 50  0000 C CNN
F 1 "180k" V 6866 2650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6680 2650 50  0001 C CNN
F 3 "~" H 6750 2650 50  0001 C CNN
	1    6750 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6450 2650 6600 2650
$Comp
L Triac_Thyristor:TIC226 Q1
U 1 1 5CEACEDD
P 7400 1950
F 0 "Q1" H 7529 1996 50  0000 L CNN
F 1 "TIC226" H 7529 1905 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7600 1875 50  0001 L CIN
F 3 "http://pdf.datasheetcatalog.com/datasheet/PowerInnovations/mXuqxvy.pdf" H 7400 1950 50  0001 L CNN
	1    7400 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5CEC160F
P 1250 1700
F 0 "J2" H 1144 1885 50  0000 C CNN
F 1 "Conn_01x02_Female" H 1144 1794 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MC-G_02x5.08mm_Angled" H 1250 1700 50  0001 C CNN
F 3 "~" H 1250 1700 50  0001 C CNN
	1    1250 1700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1750 1800 1450 1800
Text GLabel 5100 2650 0    50   Input ~ 0
SENSE
Wire Wire Line
	5100 2650 5350 2650
Text GLabel 7500 2150 2    50   Input ~ 0
SENSE
$Comp
L Device:R R7
U 1 1 5CEC2467
P 7400 2400
F 0 "R7" H 7470 2446 50  0000 L CNN
F 1 "0.05R / 5W" H 7470 2355 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0918_L18.0mm_D9.0mm_P22.86mm_Horizontal" V 7330 2400 50  0001 C CNN
F 3 "~" H 7400 2400 50  0001 C CNN
	1    7400 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2650 7100 2650
Wire Wire Line
	7100 2650 7100 2050
Wire Wire Line
	7100 2050 7250 2050
Wire Wire Line
	7400 2100 7400 2150
Wire Wire Line
	7400 2150 7500 2150
Connection ~ 7400 2150
Wire Wire Line
	7400 2150 7400 2250
Text GLabel 1750 1700 2    50   Input ~ 0
L
Wire Wire Line
	7300 1650 7400 1650
Wire Wire Line
	7400 1650 7400 1800
Text GLabel 7350 2650 0    50   Input ~ 0
N
Wire Wire Line
	7350 2650 7400 2650
Wire Wire Line
	7400 2650 7400 2550
$Comp
L Device:C C2
U 1 1 5CEC48B9
P 4700 2750
F 0 "C2" V 4448 2750 50  0000 C CNN
F 1 "3n3" V 4539 2750 50  0000 C CNN
F 2 "Capacitors_THT:C_Disc_D8.0mm_W5.0mm_P7.50mm" H 4738 2600 50  0001 C CNN
F 3 "~" H 4700 2750 50  0001 C CNN
	1    4700 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 2750 5350 2750
Text GLabel 4450 2750 0    50   Input ~ 0
N
Wire Wire Line
	4450 2750 4550 2750
$Comp
L Device:C C1
U 1 1 5CEC55BC
P 4250 2900
F 0 "C1" V 3998 2900 50  0000 C CNN
F 1 "100n" V 4089 2900 50  0000 C CNN
F 2 "Capacitors_THT:C_Disc_D8.0mm_W5.0mm_P5.00mm" H 4288 2750 50  0001 C CNN
F 3 "~" H 4250 2900 50  0001 C CNN
	1    4250 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 2850 4950 2850
Wire Wire Line
	4950 2850 4950 2900
Wire Wire Line
	4950 2900 4900 2900
Text GLabel 4000 2900 0    50   Input ~ 0
N
Wire Wire Line
	4000 2900 4100 2900
$Comp
L Device:R R2
U 1 1 5CEC75B9
P 6500 3750
F 0 "R2" H 6570 3796 50  0000 L CNN
F 1 "47k" H 6570 3705 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6430 3750 50  0001 C CNN
F 3 "~" H 6500 3750 50  0001 C CNN
	1    6500 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5CEC76DB
P 6500 4250
F 0 "RV1" H 6431 4204 50  0000 R CNN
F 1 "50k" H 6431 4295 50  0000 R CNN
F 2 "Connectors_Phoenix:PhoenixContact_MC-G_03x5.08mm_Angled" H 6500 4250 50  0001 C CNN
F 3 "~" H 6500 4250 50  0001 C CNN
	1    6500 4250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5CEC7751
P 6500 4750
F 0 "R3" H 6570 4796 50  0000 L CNN
F 1 "12k" H 6570 4705 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6430 4750 50  0001 C CNN
F 3 "~" H 6500 4750 50  0001 C CNN
	1    6500 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4600 6500 4400
Wire Wire Line
	6500 4100 6500 3900
Text GLabel 6500 5050 3    50   Input ~ 0
N
Wire Wire Line
	6500 5050 6500 4900
Wire Wire Line
	6500 3600 6500 3400
Connection ~ 6500 3400
Wire Wire Line
	6500 3400 6650 3400
Wire Wire Line
	4900 4250 4900 2900
Connection ~ 4900 2900
Wire Wire Line
	4900 2900 4400 2900
$Comp
L Connector:Conn_01x02_Female J3
U 1 1 5CEEC1E4
P 2450 1050
F 0 "J3" V 2390 1098 50  0000 L CNN
F 1 "Conn_01x02_Female" V 2299 1098 50  0000 L CNN
F 2 "Connectors_Phoenix:PhoenixContact_MC-G_02x5.08mm_Angled" H 2450 1050 50  0001 C CNN
F 3 "~" H 2450 1050 50  0001 C CNN
	1    2450 1050
	0    1    -1   0   
$EndComp
Wire Wire Line
	2550 1350 2450 1350
Wire Wire Line
	2450 1350 2450 1250
Wire Wire Line
	1450 1700 1750 1700
Text GLabel 1750 1800 2    50   Input ~ 0
Lreg
Text GLabel 7300 1650 0    50   Input ~ 0
Lreg
$Comp
L Device:Fuse F1
U 1 1 5CEEF81F
P 1900 1250
F 0 "F1" V 1703 1250 50  0000 C CNN
F 1 "2A" V 1794 1250 50  0000 C CNN
F 2 "Fuse_Holders_and_Fuses:Fuseholder5x20_horiz_SemiClosed_Casing10x25mm" V 1830 1250 50  0001 C CNN
F 3 "~" H 1900 1250 50  0001 C CNN
	1    1900 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	1450 1250 1750 1250
Wire Wire Line
	2050 1250 2350 1250
Text GLabel 5200 3600 3    50   Input ~ 0
N
Wire Wire Line
	5200 3600 5200 3400
Connection ~ 5200 3400
$Comp
L Device:R_Variable R1
U 1 1 5CEF2E6A
P 5700 4250
F 0 "R1" V 5965 4250 50  0000 C CNN
F 1 "100k" V 5874 4250 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" V 5630 4250 50  0001 C CNN
F 3 "~" H 5700 4250 50  0001 C CNN
	1    5700 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 4250 5550 4250
Wire Wire Line
	5850 4250 6350 4250
$EndSCHEMATC
